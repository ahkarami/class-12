package sbu.cs.hint;

import org.postgresql.util.PSQLException;

import java.io.Closeable;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLDatabaseConnection implements Closeable {

    private static final String CONN_URL = "jdbc:postgresql://localhost:5432/test";
    private final Connection conn;

    public SQLDatabaseConnection(String tableName) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        this.conn = DriverManager.getConnection(CONN_URL, "ap", "ap");
        initiateUserTable(tableName);
    }

    public void initiateUserTable(String tableName) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (\n" +
                "  username varchar(45) NOT NULL,\n" +
                "  password varchar(65) NOT NULL,\n" +
                "  PRIMARY KEY (username)\n" +
                ")";
        Statement stmt = conn.createStatement();
        stmt.execute(sql);
        System.out.printf("table %s initiated %n", tableName);
        stmt.close();
    }

    public void addPlayer(String tableName, String username, String password) throws SQLException {
        String sql = "INSERT INTO " + tableName + " VALUES (?, ?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, username);
        ps.setString(2, password);
        System.out.println(ps);
        int i = 0;
        try {
            i = ps.executeUpdate();
        } catch (PSQLException e) {
            System.out.println("error in adding player. " + e.getMessage());
        }
        ps.close();
        System.out.printf("%d rows effected %n", i);
    }

    public List<Player> getPlayers(String tableName) throws SQLException {
        String sql = "SELECT * FROM " + tableName;
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<Player> players = new ArrayList<>();
        while (resultSet.next()) {
            players.add(new Player(resultSet.getString(1), resultSet.getString(2)));
        }
        statement.close();
        return players;
    }

    public Player getPlayer(String tableName, String username) throws SQLException {
        String sql = "SELECT * FROM " + tableName;
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        Player player = new Player();
        while (resultSet.next()) {
            player.setUsername(resultSet.getString(1));
            player.setPassword(resultSet.getString(2));
        }
        statement.close();
        return player;
    }

    @Override
    public void close() throws IOException {
        try {
            if (!this.conn.isClosed()) {
                this.conn.close();
            }
        } catch (SQLException error) {
            error.printStackTrace();
        }
    }
}
