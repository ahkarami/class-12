package sbu.cs.hint;

import com.mongodb.DBObject;

public class Player {

    private String username;
    private String password;

    public Player() {
    }

    public Player(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static Player parsePlayer(DBObject object) {
        Player player = new Player();
        player.setUsername((String) object.get("username"));
        player.setPassword((String) object.get("password"));
        return player;
    }

    @Override
    public String toString() {
        return "Player{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
