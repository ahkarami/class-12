package sbu.cs.real;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class App {

    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {
        String tableName = "myPlayers";
//        SQLConnection sql = new SQLConnection(tableName);
//
//        Player player1 = new Player("morteza", "123");
//        sql.insertPlayer(player1);
//        List<Player> players = sql.getPlayers();
//        for (Player player : players) {
//            System.out.println(player);
//        }
//        System.out.println(sql.getPlayer("ali"));
//        System.out.println(sql.getPlayer("sahar"));
//        System.out.println(sql.getPlayer("amir"));
//
//        sql.close();

        Player player2 = new Player("ali", "qwertyhn");
        String databaseName = "instagram";
        MongoConnection mongo = new MongoConnection(databaseName);
        mongo.insertToCollection(tableName, player2.getDBObject());
//        List<Player> players = mongo.getPlayers(tableName);
//        for (Player player : players) {
//            System.out.println(player);
//        }
        System.out.println(mongo.getPlayer(tableName, "sahar"));
    }
}
